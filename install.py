import os
import sys
import time
import distro
import platform
from threading import Thread
# Our files
import st
import dwm
import theme 

wm  = 0
deb = 0

if platform.system() == 'Linux':
    pass
else:
    exit("You need to run this under Linux !\n Please try again, this time under Linux. Exiting.")
def clr():
    os.system("clear")

def loading():
    load = True
    while load==True:
        clr()
        print("Please Wait |")
        time.sleep(1)
        clr()
        print("Please Wait /")
        time.sleep(1)
        clr()
        print("Please Wait -")
        time.sleep(1)
        clr()
        print("Please Wait \ ")
        time.sleep(1)
        clr()
        print("Please Wait |")
        clr()
        time.sleep(1)
    

def main():
    clr()
    print("Welcome to V64Deployer !")
    time.sleep(1)
def checks():
    # Check root permissions
    if os.geteuid() == 0:
        exit("It's dangerous to have root privileges to run this script.\nPlease try again.")
    else:
        pass
    
    clr()
    # Tell the user his distro
    print("We think your OS is ", distro.id())
    print("Is that correct ?\n1.Yes 2.No")
    global deb
    prompt = int(sys.stdin.readline())

    # If that's not correct, ask if the distro is based on arch or debian
    #if deb == "no" or deb == "NO" or deb == "No" or deb == "nO" or deb == 0:
    if prompt == 2:
        print("Is your distro debian-based ? (ubuntu, linuxmint, etc ...)\n1.Yes 2.No")
        prompt = int(sys.stdin.readline())
        if prompt == 1:
            deb = 1
            print("Your distro is based on Debian")
            print("Okay ! Let's get started !")
        elif prompt == 2:
            deb = 0
            print("Your distro is based on Arch")
            print("Okay ! Let's get started !")
    elif prompt == 1:
        print("Okay ! Let's get started !")
    
    #print("There are 2 WM that can be installed : 1. Awesome - 2. Suckless DWM")
    #global wm 
    #wm = int(sys.stdin.readline())
    #if wm == 1:
    #    wm = 1 # Awesome
    #elif wm == 2:
     #   wm = 2 # DWM
    #else:
     #   exit("Sorry, an error occured, please try running this script again!")

    print("Okay ! Sounds good !")

# Begin install

terminal    = st.InstallSt()
xServer     = dwm.InstallXorg()
gtktheme    = theme.Theming()
#if wm == 1: 
#    wm      = dwm.InstallAwesome()
#elif wm == 2:
wm      = dwm.InstallDwm()

def install():
    # Terminal
    terminal.cloneSt()
    terminal.installSt()
    # Xorg
    xServer.installX(deb)
    # Theming
    gtktheme.gtkTheme()
    gtktheme.fontTheme()
    # Window Manager
    wm.cloneWM()
    wm.installWM()

    load = False
main()
checks()
#Thread(target = loading).start()
Thread(target = install).start()
