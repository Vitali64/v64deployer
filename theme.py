import os
import sys

class Theming():
    def __init__(self):
        pass
    def gtkTheme(self):
        os.system("mkdir ~/.themes")
        os.system("cp -r src/Midnight-Gray ~/.themes")
    def fontTheme(self):
        os.system("mkdir ~/.local/share/fonts")
        os.system("cp -r src/JetBrains\ Mono\ Regular\ Nerd\ Font\ Complete\ Mono.ttf ~/.local/share/fonts")

