import os
import sys
import time
import platform
import distro

class InstallXorg:
    def __init__(self):
        pass
    def installX(self, deb):
        if distro.id() == "ubuntu" or distro.id() == "debian" or distro.id() == "raspbian" or distro.id() == "linuxmint" or deb == 1:
            os.system("sudo apt-get install xorg-xserver")
        elif distro.id() == "arch" or distro.id() == "artix" or distro.id() == "manjaro" or deb == 0:
            os.system("sudo pacman -S xorg-server --needed --noconfirm >> report.log")

class InstallDwm:
    def __init__(self):
        pass
    def cloneWM(self):
        os.system("git clone https://gitlab.com/vitali64/dots/v64-dwm.git ~/.config/dwm-src >> report.log")
    def installWM(self):
        os.system("cd ~/.config/dwm-src && make && cd ~")
        

#class InstallAwesome:
#    def __init__(self):
#        global deb
#    def cloneWM(self):
#        os.system("git clone https://gitlab.com/vitali64/dots/pop-some ~/.config/awesome/")
#    def installWM(self, deb):
#        if distro.id() == "ubuntu" or distro.id() == "debian" or distro.id() == "raspbian" or distro.id() == "linuxmint" or deb == 1:
#            os.system("sudo apt-get install awesome -y")
#        elif distro.id() == "arch" or distro.id() == "artix" or distro.id() == "manjaro" or deb == 0:
#            os.system("sudo pacman -S awesome --noconfirm")


